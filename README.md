# rktsetwacom

UI for creating the xsetwacom command for custom active-area size for wacom tablets.

https://gitlab.com/juszczakn/rktxsetwacom

# Use

Start up either using `racket main.rkt` or via the executable.

Launches a UI for setting up a custom x-y active area. When done, copy the command
and run it in a terminal to set the active-area.

![Example](doc/rktsetwacom.png)
